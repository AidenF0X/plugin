package net.bor.template;

import java.util.Arrays;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author ArtBorax
 */
public class ExampleCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender src, Command cmnd, String string, String[] args) {
        if (!(src instanceof Player)) {
            //источник не игрок
            return true;
        }
        System.out.println(string);
        System.out.println(Arrays.toString(args));

        return true;
    }
}
